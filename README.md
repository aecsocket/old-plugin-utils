# PluginUtils

Generic utils for plugins

---

This plugin contains utilities such as JSON parsing methods (using GSON), loops and more.

## For server owners

Put the JAR in `$SERVER/plugins` and it will work if any plugins require it.

## For developers

Javadoc is available at [src/main/javadoc](src/main/javadoc).

Each class has a description of its use, so the best way to see what to use is to just look at the javadoc.

### Loops

Create a new instance of a `me.aecsocket.pluginutils.loop.Loop` class in your plugin, and start registering your own
objects to it which implement `me.aecsocket.pluginutils.loop.Tickable` using `Loop#registerTickable(Tickable)`. Then,
start the loop using `Loop#start()`.

### JSON

`me.aecsocket.pluginutils.json.JsonAdapter` combines a `JSONSerializer` and `JSONDeserializer` as a helper interface.
However, there are also premade adapters for generic Spigot classes that you can find in
`me.aecsocket.pluginutils.json.spigot.SpigotAdapters`. Currently there is:

- LocationAdapter
- VectorAdapter

Included in the `SpigotAdapters` class is also a custom `GSONBuilder` object which has all Spigot JSON type adapters registered
into it.

`me.aecsocket.pluginutils.json.JsonUtils` also has a variety of JSON utilities, all with error handling.

## Download

Currently there is no JAR download available. You must compile it yourself.
