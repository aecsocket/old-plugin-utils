package me.aecsocket.pluginutils;

import me.aecsocket.pluginutils.exception.ConfigurationException;
import me.aecsocket.pluginutils.util.GlowEnchant;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;

/** The main plugin class. */
public class PluginUtils extends JavaPlugin {
    private static PluginUtils instance;

    public static PluginUtils getInstance() { return instance; }

    @Override
    public void onEnable() {
        instance = this;

        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);

            Enchantment.registerEnchantment(new GlowEnchant());

            f.setAccessible(false);
        }
        catch (Exception ignore) { }

    }

    /** Gets a config value from a plugin's config.
     @param plugin The {@link Plugin}.
     @param path The path to the key.
     @param type The type of the result.
     @param <T> The type of the result.
     @return The config value.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getConfigValue(Plugin plugin, String path, Class<T> type) {
        FileConfiguration config = plugin.getConfig();
        if (config.contains(path))
            return (T)config.get(path);
        else
            throw new ConfigurationException(String.format("No key '%s' found in '%s' config", path, plugin.getName()));
    }
}
