package me.aecsocket.pluginutils.colornamed;

import org.bukkit.ChatColor;

/** An object which both has a name and a defined set of {@link org.bukkit.ChatColor}s to format it. */
public interface ColorNamed {
    /** Gets the name.
     * @return The name.
     */
    String getName();

    /** Gets the {@link org.bukkit.ChatColor}s to format the name.
     * @return The {@link org.bukkit.ChatColor}s to format the name.
     */
    ChatColor[] getColors();

    /** Gets the fully formatted name.
     <p>
     Consider using {@link ColorNamedUtils#getFormattedName(ColorNamed, ChatColor...)}.
     * @param extraFormatting If applicable, extra {@link ChatColor}s to format the name with.
     * @return The fully formatted name.
     */
    String getFormattedName(ChatColor... extraFormatting);
}
