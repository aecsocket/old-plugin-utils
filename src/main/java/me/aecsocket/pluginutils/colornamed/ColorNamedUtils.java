package me.aecsocket.pluginutils.colornamed;

import org.bukkit.ChatColor;

/** Utils for classes extending {@link me.aecsocket.pluginutils.colornamed.ColorNamed}. */
public final class ColorNamedUtils {
    /** Gets the fully formatted name of a {@link ColorNamed}.
     * @param object The {@link ColorNamed}.
     * @param extraFormatting If applicable, extra {@link ChatColor}s to format the name with.
     * @return The fully formatted name.
     */
    public static String getFormattedName(ColorNamed object, ChatColor... extraFormatting) {
        StringBuilder str = new StringBuilder();

        for (ChatColor col : object.getColors())
            str.append(col);
        for (ChatColor col : extraFormatting)
            str.append(col);

        str.append(object.getName());
        return str.toString();
    }
}
