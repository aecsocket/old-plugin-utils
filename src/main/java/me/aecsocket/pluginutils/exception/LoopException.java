package me.aecsocket.pluginutils.exception;

public class LoopException extends RuntimeException {
    public LoopException(String message) {
        super(message);
    }
}
