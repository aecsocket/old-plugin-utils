package me.aecsocket.pluginutils.json;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;

/** A {@link JsonSerializer} and {@link JsonDeserializer}. */
public interface JsonAdapter<T> extends JsonSerializer<T>, JsonDeserializer<T> { }
