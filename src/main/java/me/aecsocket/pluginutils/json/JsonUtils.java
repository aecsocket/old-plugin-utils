package me.aecsocket.pluginutils.json;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;

/** Generic Google JSON utilities. */
public final class JsonUtils {
    /** Gets a {@link JsonElement} from a {@link JsonObject} and handles exceptions.
     @param json The {@link JsonObject}.
     @param memberName The name of the member in the {@link JsonObject}.
     @return The {@link JsonElement}
     @throws JsonParseException If the member does not exist.
     */
    public static JsonElement get(JsonObject json, String memberName) throws JsonParseException {
        if (json.has(memberName))
            return json.get(memberName);
        throw new JsonParseException(String.format("Missing member '%s'", memberName));
    }

    /** Gets a {@link JsonElement} from a {@link JsonObject} if it exists, otherwise returns a default.
     @param json The {@link JsonObject}.
     @param memberName The name of the member in the {@link JsonObject}.
     @param original The default {@link JsonElement}.
     @return The {@link JsonElement}.
     */
    public static JsonElement getOrDefault(JsonObject json, String memberName, JsonElement original) {
        if (json.has(memberName))
            return json.get(memberName);
        return original;
    }

    /** Asserts that a {@link JsonElement} is a {@link JsonObject}.
     @param json The {@link JsonElement}.
     @return The {@link JsonElement} in {@link JsonObject} form.
     @throws JsonParseException If it is not a {@link JsonObject}.
     */
    public static JsonObject assertObject(JsonElement json) throws JsonParseException {
        if (json.isJsonObject())
            return json.getAsJsonObject();
        throw new JsonParseException("Element must be object");
    }

    /** Asserts that a {@link JsonElement} is a {@link JsonArray}.
     @param json The {@link JsonElement}.
     @return The {@link JsonElement} in {@link JsonArray} form.
     @throws JsonParseException If it is not a {@link JsonArray}.
     */
    public static JsonArray assertArray(JsonElement json) throws JsonParseException {
        if (json.isJsonArray())
            return json.getAsJsonArray();
        throw new JsonParseException("Element must be array");
    }

    /** Asserts that a {@link JsonElement} is a {@link JsonPrimitive}.
     @param json The {@link JsonElement}.
     @return The {@link JsonElement} in {@link JsonPrimitive} form.
     @throws JsonParseException If it is not a {@link JsonPrimitive}.
     */
    public static JsonPrimitive assertPrimitive(JsonElement json) throws JsonParseException {
        if (json.isJsonPrimitive())
            return json.getAsJsonPrimitive();
        throw new JsonParseException("Element must be primitive");
    }

    /** Asserts that a {@link JsonElement} is a {@link JsonNull}.
     @param json The {@link JsonElement}.
     @return The {@link JsonElement} in {@link JsonNull} form.
     @throws JsonParseException If it is not a {@link JsonNull}.
     */
    public static JsonNull assertNull(JsonElement json) throws JsonParseException {
        if (json.isJsonNull())
            return json.getAsJsonNull();
        throw new JsonParseException("Element must be null");
    }

    /** Gets an {@link Enum} value from a {@link com.google.gson.JsonObject} member and handles exceptions.
     @param json The {@link JsonObject}.
     @param memberName The name of the member in the {@link JsonObject}.
     @param enumType The type of the {@link Enum}.
     @param <T> The type of the {@link Enum}.
     @return The {@link Enum} value.
     @throws JsonParseException If the member is not a string, or if it is an invalid enum value.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Enum> T getEnum(JsonObject json, String memberName, Class<T> enumType) throws JsonParseException {
        JsonElement elem = get(json, memberName);
        JsonPrimitive primitive = assertPrimitive(elem);
        if (primitive.isString()) {
            String enumName = primitive.getAsString();
            try {
                return (T)Enum.valueOf(enumType, enumName);
            } catch (IllegalArgumentException e) {
                throw new JsonParseException(String.format("Invalid enum value '%s'", enumName));
            }
        }
        throw new JsonParseException("Enum value must be string");
    }

    /** Converts a {@link JsonArray} to a {@link List}.
     @param array The {@link JsonArray}.
     @return The {@link List}.
     */
    public static List<String> toList(JsonArray array) {
        List<String> list = new ArrayList<>();

        for (int i = 0; i < array.size(); i++)
            list.add(array.get(0).getAsString());

        return list;
    }
}
