package me.aecsocket.pluginutils.json.spigot;

import com.google.gson.*;
import me.aecsocket.pluginutils.json.JsonAdapter;
import me.aecsocket.pluginutils.json.JsonUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.lang.reflect.Type;

public class LocationAdapter implements JsonAdapter<Location> {
    @Override
    public JsonElement serialize(Location object, Type type, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.add("world", new JsonPrimitive(object.getWorld().getName()));
        json.add("x", new JsonPrimitive(object.getX()));
        json.add("y", new JsonPrimitive(object.getY()));
        json.add("z", new JsonPrimitive(object.getZ()));
        float pitch = object.getPitch();
        if (pitch != 0)
            json.add("pitch", new JsonPrimitive(pitch));
        float yaw = object.getYaw();
        if (yaw != 0)
            json.add("yaw", new JsonPrimitive(yaw));

        return json;
    }

    @Override
    public Location deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = JsonUtils.assertObject(json);
        String world = JsonUtils.get(jsonObject, "world").getAsString();
        double x = JsonUtils.get(jsonObject, "x").getAsDouble();
        double y = JsonUtils.get(jsonObject, "y").getAsDouble();
        double z = JsonUtils.get(jsonObject, "z").getAsDouble();
        float pitch = JsonUtils.getOrDefault(jsonObject, "pitch", new JsonPrimitive(0.0f)).getAsFloat();
        float yaw = JsonUtils.getOrDefault(jsonObject, "yaw", new JsonPrimitive(0.0f)).getAsFloat();

        return new Location(Bukkit.getWorld(world), x, y, z, pitch, yaw);
    }
}
