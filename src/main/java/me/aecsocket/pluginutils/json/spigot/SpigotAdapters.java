package me.aecsocket.pluginutils.json.spigot;

import com.google.gson.GsonBuilder;
import org.bukkit.Location;
import org.bukkit.util.Vector;

/** A collection of {@link me.aecsocket.pluginutils.json.JsonAdapter}s for base Spigot classes. */
public final class SpigotAdapters {
    public static final VectorAdapter VECTOR_ADAPTER = new VectorAdapter();
    public static final LocationAdapter LOCATION_ADAPTER = new LocationAdapter();
    public static final GsonBuilder GSON_BUILDER = new GsonBuilder()
            .registerTypeAdapter(Vector.class, VECTOR_ADAPTER)
            .registerTypeAdapter(Location.class, LOCATION_ADAPTER);
}
