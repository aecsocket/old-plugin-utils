package me.aecsocket.pluginutils.json.spigot;

import com.google.gson.*;
import me.aecsocket.pluginutils.json.JsonAdapter;
import me.aecsocket.pluginutils.json.JsonUtils;
import org.bukkit.util.Vector;

import java.lang.reflect.Type;

public class VectorAdapter implements JsonAdapter<Vector> {
    @Override
    public JsonElement serialize(Vector object, Type type, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.add("x", new JsonPrimitive(object.getX()));
        json.add("y", new JsonPrimitive(object.getY()));
        json.add("z", new JsonPrimitive(object.getZ()));

        return json;
    }

    @Override
    public Vector deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = JsonUtils.assertObject(json);
        double x = JsonUtils.get(jsonObject, "x").getAsDouble();
        double y = JsonUtils.get(jsonObject, "y").getAsDouble();
        double z = JsonUtils.get(jsonObject, "z").getAsDouble();

        return new Vector(x, y, z);
    }
}
