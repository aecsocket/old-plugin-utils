package me.aecsocket.pluginutils.loop;

import me.aecsocket.pluginutils.exception.LoopException;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/** An object responsible for ticking {@link Tickable}s. */
public class Loop {
    /** The plugin this instance belongs to. */
    private final Plugin plugin;

    /** How many ticks this has been running for. */
    private long ticks;
    /** What {@link Tickable}s this handles. */
    private final List<Tickable> tickables;
    /** What task ID this has been assigned by the {@link org.bukkit.scheduler.BukkitScheduler}. */
    private int task;

    public Loop(Plugin plugin) {
        this.plugin = plugin;
        this.ticks = 0;
        this.tickables = new ArrayList<>();
        this.task = -1;
    }

    /** Gets the plugin this instance belongs to.
     @return The plugin this instance belongs to.
     */
    public Plugin getPlugin() { return plugin; }

    /** Gets how many ticks this has been running for.
     @return How many ticks this has been running for.
     */
    public long getTicks() { return ticks; }

    /** Gets what {@link Tickable}s this handles.
     @return What {@link Tickable}s this handles.
     */
    public List<Tickable> getTickables() { return tickables; }

    /** Gets what task ID this has been assigned by the {@link org.bukkit.scheduler.BukkitScheduler}.
     @return What task ID this has been assigned by the {@link org.bukkit.scheduler.BukkitScheduler}.
     */
    public int getTask() { return task; }

    /** Gets if the loop is running.
     @return If the loop is running.
     */
    public boolean isRunning() { return task != -1; }

    /** Registers a {@link Tickable} in.
     @param tickable The {@link Tickable} to register.
     */
    public void registerTickable(Tickable tickable) { tickables.add(tickable); }

    /** Unregisters a {@link Tickable}.
     @param tickable The {@link Tickable} to unregister.
     */
    public void unregisterTickable(Tickable tickable) { tickables.remove(tickable); }

    /** Starts the loop via the {@link org.bukkit.scheduler.BukkitScheduler}. */
    public void start() {
        if (isRunning())
            throw new LoopException("Loop is already running");
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this::tick, 1L, 1L);
    }

    /** Stops the loop if it is running via the {@link org.bukkit.scheduler.BukkitScheduler}. */
    public void stop() {
        if (isRunning())
            Bukkit.getScheduler().cancelTask(task);
        throw new LoopException("Loop is not running");
    }

    /** Restarts the loop if it is running via the {@link org.bukkit.scheduler.BukkitScheduler}. */
    public void restart() {
        if (isRunning()) {
            stop();
            start();
        }
        throw new LoopException("Loop is not running");
    }

    /** Increases the tick count and ticks all {@link me.aecsocket.pluginutils.loop.Tickable}s that are registered to
     it. */
    public void tick() {
        ++ticks;
        List<Tickable> remove = new ArrayList<>();
        for (Tickable tickable : tickables) {
            if (tickable.tick(this))
                remove.add(tickable);
        }
        tickables.removeAll(remove);
    }
}
