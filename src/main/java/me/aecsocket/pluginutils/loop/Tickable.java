package me.aecsocket.pluginutils.loop;

/** An object which can be ticked by the main {@link me.aecsocket.pluginutils.loop.Loop}. */
public interface Tickable {
    boolean tick(Loop loop);
}
