package me.aecsocket.pluginutils.util;

import me.aecsocket.pluginutils.PluginUtils;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

/** A custom enchantment which does not have a name, but can be used to apply a glowing effect to {@link org.bukkit.inventory.ItemStack}s. */
public class GlowEnchant extends Enchantment {
    public GlowEnchant() {
        super(new NamespacedKey(PluginUtils.getInstance(), "glow"));
    }

    @SuppressWarnings("deprecation")
    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 0;
    }

    @Override
    public int getStartLevel() {
        return 0;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public boolean isTreasure() {
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean isCursed() {
        return false;
    }

    @Override
    public boolean conflictsWith(Enchantment enchantment) {
        return false;
    }

    @Override
    public boolean canEnchantItem(ItemStack itemStack) {
        return true;
    }
}
