package me.aecsocket.pluginutils.util;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

/** Defines most fields of #spawnParticle methods. */
public class ParticleData implements Cloneable {
    private Particle particle;
    private int count;
    private double sizeX;
    private double sizeY;
    private double sizeZ;
    private double speed;
    private Object data;
    private boolean force;

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed, Object data, boolean force) {
        this.particle = particle;
        this.count = count;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.speed = speed;
        this.data = data;
        this.force = force;
    }

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed, Object data) {
        this(particle, count, sizeX, sizeY, sizeZ, speed, data, false);
    }

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed) {
        this(particle, count, sizeX, sizeY, sizeZ, speed, null, false);
    }

    public ParticleData(Particle particle, int count, double size, double speed, Object data) {
        this(particle, count, size, size, size, speed, data, false);
    }

    public ParticleData(Particle particle, int count, double size, double speed) {
        this(particle, count, size, size, size, speed, null, false);
    }

    public Particle getParticle() { return particle; }
    public void setParticle(Particle particle) { this.particle = particle; }

    public int getCount() { return count; }
    public void setCount(int count) { this.count = count; }

    public double getSizeX() { return sizeX; }
    public void setSizeX(double sizeX) { this.sizeX = sizeX; }

    public double getSizeY() { return sizeY; }
    public void setSizeY(double sizeY) { this.sizeY = sizeY; }

    public double getSizeZ() { return sizeZ; }
    public void setSizeZ(double sizeZ) { this.sizeZ = sizeZ; }

    public void setSize(double size) {
        sizeX = size;
        sizeY = size;
        sizeZ = size;
    }

    public double getSpeed() { return speed; }
    public void setSpeed(double speed) { this.speed = speed; }

    public Object getData() { return data; }
    public void setData(Object data) { this.data = data; }

    public boolean force() { return force; }
    public void setForce(boolean force) { this.force = force; }

    public void spawn(Location location) {
        location.getWorld().spawnParticle(particle, location, count, sizeX, sizeY, sizeZ, speed, data, force);
    }

    public void spawn(Location location, Player player) {
        player.spawnParticle(particle, location, count, sizeX, sizeY, sizeZ, speed, data);
    }

    @Override
    public ParticleData clone() { try { return (ParticleData)super.clone(); } catch (CloneNotSupportedException e) { return null; } }
}
