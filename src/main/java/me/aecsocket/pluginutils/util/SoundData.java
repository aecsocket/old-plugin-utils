package me.aecsocket.pluginutils.util;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;

/** Defines most fields of #playSound methods. */
public class SoundData implements Cloneable {
    private String sound;
    private SoundCategory category;
    private float volume;
    private float pitch;

    public SoundData(String sound, SoundCategory category, float volume, float pitch) {
        this.sound = sound;
        this.category = category;
        this.volume = volume;
        this.pitch = pitch;
    }

    public SoundData(Sound sound, SoundCategory category, float volume, float pitch) {
        this(soundToString(sound), category, volume, pitch);
    }

    public void play(Location location) {
        location.getWorld().playSound(location, sound, category, volume, pitch);
    }

    public void play(Location location, Player player) {
        player.playSound(location, sound, category, volume, pitch);
    }

    @Override
    public SoundData clone() { try { return (SoundData)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    // Only a general rule
    public static String soundToString(Sound sound) { return sound.name().toLowerCase().replace('_', '.'); }
}
