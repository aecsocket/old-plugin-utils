package me.aecsocket.pluginutils.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/** Generic utilities. */
public final class Utils {
    /** Converts an {@link Object} array to a {@link String} formatted as <code>[element1, element2, ...]</code>.
     @param array The {@link Object} array.
     @return The {@link String}.
     */
    public static String arrayToString(Object[] array) {
        if (array != null) {
            List<String> strings = new ArrayList<>();

            for (Object o : array)
                strings.add(o == null ? "null" : o.toString());

            return String.format("[%s]", String.join(",", strings));
        }
        return null;
    }

    /** Gets a toString representation of an {@link Object}.
     * @param object The {@link Object}.
     * @return The toString representation.
     */
    public static String getToString(Object object) {
        Class<?> clazz = object.getClass();

        List<String> args = new ArrayList<>();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            try {
                args.add(String.format("%s=%s", field.getName(), field.get(object)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            field.setAccessible(false);
        }

        return String.format("%s{%s}", clazz.getSimpleName(), String.join(", ", args));
    }
}
