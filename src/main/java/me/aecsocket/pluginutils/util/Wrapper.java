package me.aecsocket.pluginutils.util;

/** A wrapper around another class.
 @param <T> The class to wrap around.
 */
public class Wrapper<T> {
    /** The original {T}. */
    private final T handle;

    public Wrapper(T handle) {
        this.handle = handle;
    }

    /** Gets the original {T}.
     @return The original {T}
     */
    public T getHandle() { return handle; }
}
